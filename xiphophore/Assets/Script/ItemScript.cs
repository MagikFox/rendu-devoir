using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Item", order = 1)]
public class ItemScript : ScriptableObject
{
    public string Name;
    public int ID;
    public Color Color;
}
