using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IPointerDownHandler, IDragHandler, IDropHandler 
{
    public ItemScript Itemscript;
    public Recipe Recette;
    public string Name;
    CanvasGroup CG;
    private void Start()
    {
        Image image = GetComponent<Image>();
        image.color = Itemscript.Color;
        name = Itemscript.name;
        Name = name;
        CG = GetComponent<CanvasGroup>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        CG.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        CG.blocksRaycasts = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(name);

        Item item = eventData.pointerDrag.GetComponent<Item>();
        for (int i = 0; i < Recette.Couleur1.Length; i++)
        {
            Debug.Log(item.Name + " " + Name);
            if (Recette.Couleur1[i].Name == Name || Recette.Couleur1[i].Name == item.Name)
            {
                Debug.Log("if 1");
                if (Recette.Couleur2[i].Name == Name || Recette.Couleur2[i].Name == item.Name)
                {
                    Debug.Log("if 2");
                    Instantiate(Recette.CouleurResult[i]);
                    transform.DetachChildren();
                    Destroy(item.gameObject);
                    Destroy(this.gameObject);
                }
            }
        }

    }
}
