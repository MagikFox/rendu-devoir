using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Recipe", menuName = "ScriptableObjects/Recipe", order = 1)]
public class Recipe : ScriptableObject
{
    public ItemScript[] Couleur1;
    public ItemScript[] Couleur2;

    public GameObject[] CouleurResult;
}
